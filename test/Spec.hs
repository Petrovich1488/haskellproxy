{-# LANGUAGE OverloadedStrings #-}

module Main where

import Prelude hiding (getContents)
import System.Environment (getArgs)
import System.IO (Handle, hSetBuffering, BufferMode(NoBuffering), IOMode(ReadMode), hClose)

import Network (listenOn, accept, PortID(..), Socket) 
import Network.Socket hiding (accept, recv)
import Network.Socket.ByteString (recv, sendAll)
import Network.BSD (getHostByName, hostAddress)

import Control.Concurrent (forkIO)

import Data.Monoid
import Data.List.Split
import Data.Maybe
import Data.Attoparsec.ByteString (maybeResult, parseWith, parse)
import Data.ByteString.Char8 (putStr, pack, unpack)
import qualified Data.ByteString as BS

import Types
import HttpParser
import HttpPrinter


main :: IO ()
main = do
    let a = pack "HTTP/1.1 206 Partial Content\r\nLast-Modified: Thu, 02 Feb 2017 11:15:53 GMT\r\nETag: \"18c50e1bbe972bdf9c7d9b8f6f019959\"\r\nx-amz-storage-class: STANDARD_IA\r\nContent-Type: image/jpeg\r\ncache-control: public, max-age=31536000\r\nAccept-Ranges: bytes\r\nDate: Tue, 02 Oct 2018 21:43:19 GMT\r\nAge: 725584\r\nConnection: keep-alive\r\nX-Served-By: cache-iad2136-IAD, cache-hel6821-HEL\r\nX-Cache: HIT, HIT\r\nX-Cache-Hits: 1, 19\r\nX-Timer: S1538516599.010928,VS0,VE0\r\nAccess-Control-Allow-Methods: GET, OPTIONS\r\nAccess-Control-Allow-Origin: *\r\nServer: cat factory 1.0\r\nContent-Range: bytes 0-1023/146515\r\nContent-Length: 420\r\n\r\n\255\216\255\219\NULC\NUL\ETX\STX\STX\ETX\STX\STX\ETX\ETX\ETX\ETX\EOT\ETX\ETX\EOT\ENQ\b\ENQ\ENQ\EOT\EOT\ENQ\n\a\a\ACK\b\f\n\f\f\v\n\v\v\r\SO\DC2\DLE\r\SO\DC1\SO\v\v\DLE\SYN\DLE\DC1\DC3\DC4\NAK\NAK\NAK\f\SI\ETB\CAN\SYN\DC4\CAN\DC2\DC4\NAK\DC4\255\219\NULC\SOH\ETX\EOT\EOT\ENQ\EOT\ENQ\t\ENQ\ENQ\t\DC4\r\v\r\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\DC4\255\192\NUL\DC1\b\EOT\NUL\STX\171\ETX\SOH\DC1\NUL\STX\DC1\SOH\ETX\DC1\SOH\255\196\NUL\RS\NUL\NUL\SOH\EOT\ETX\SOH\SOH\SOH\NUL\NUL\NUL\NUL\NUL\NUL\NUL\NUL\NUL\ENQ\NUL\ETX\EOT\ACK\SOH\STX\a\b\t\n\255\196\NULM\DLE\NUL\SOH\ETX\ETX\ETX\STX\ENQ\STX\ETX\ACK\EOT\EOT\ENQ\STX\NUL\SI\SOH\STX\ETX\DC1\NUL\EOT\ENQ\DC2!1\ACKA\a\DC3\"Qaq\129\DC42\145\b#B\161\177\193\NAKR\209\240\SYN3b\225\t$r\130\241CS\162\ETBc\146\CAN\131%&45s\147D\178\226\255\196\NUL\ESC\SOH\NUL\ETX\SOH\SOH\SOH\SOH\SOH\NUL\NUL\NUL\NUL\NUL\NUL\NUL\NUL\NUL\NUL\SOH\STX\ETX\EOT\ENQ\ACK\a\255\196\NUL4\DC1\SOH\SOH\SOH\NUL\STX\STX\STX\STX\SOH\ETX\ETX\ETX\STX\ENQ\ENQ\NUL\NUL\SOH\DC1\STX!\DC21\ETXAQaq\EOT\DC3\"\ENQ\129\240\&2\145\161\177\193\DC4#B\225\241\NAK3Rb\209\255\218\NUL\f\ETX\SOH\NUL\STX\DC1\ETX\DC1\NUL?\NUL\249U@*\SOHP\n\128T\STX\160\NAK\NUL\168\ENQ@*\SOHP\n\128T\STX\160\NAK\NUL\168\ENQ@*\SOHP\n\128T\STX\160\NAK\NUL\168\ENQ@*\SOHP\n\128T\STX\160\NAK\NUL\168"
    print $
     printResponse $
      fromMaybe undefined $
       maybeResult $ 
       parse response a
