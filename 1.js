var http = require('http');
	
http.createServer(function (request, response) {
	response.setHeader('Content-Type', 'text/html; charset=UTF-8');
	response.setHeader('Transfer-Encoding', 'chunked');
	
	var html =
	'<!DOCTYPE html>' +
	'<html lang="en">' +
	'<head>' +
	'<meta charset="utf-8">' +
	'<title>Chunked transfer encoding test</title>' +
	'</head>' +
	'<body>';
	
	response.write(html);
	
	html = '<h1>Chunked transfer encoding test</h1>'
	
	response.write(html);
	
	html = '<h5>This is a chunked response </h5>'
	
	response.write(html);
	
	html = '<h5>This is a chunked response. The server should not close the stream before all chunks are sent to a client.</h5>'
	
	response.write(html);
	
	// since this is the last chunk, close the stream.
	html =
	'</body>' +
	'</html>';
	
	response.end(html);
	
}).listen(process.env.VMC_APP_PORT || 1337, null);
