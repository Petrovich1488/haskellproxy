{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}

module Main where

import Prelude hiding (lookup)
import System.Environment (getArgs)
import System.IO (Handle, hSetBuffering, BufferMode(..), IOMode(..), hClose)

import Network (listenOn, accept, PortID(..), Socket) 
import Network.Socket hiding (accept)
import Network.Socket.ByteString (recv, sendAll)
import Network.BSD (getHostByName, hostAddress)
import Network.HTTP.Date

import Control.Concurrent (forkIO)

import Data.Monoid
import Data.Time.Clock
import Data.Time.Calendar
import Data.List.Split
import Data.Maybe
import Data.Attoparsec.ByteString (maybeResult, parseWith, parse)
import Data.ByteString.Char8 (putStr, pack, unpack)
import qualified Data.ByteString as BS
import Data.Cache.LRU.IO

import Types
import HttpParser
import HttpPrinter

newCache :: IO (AtomicLRU BS.ByteString HttpResponse)
newCache = newAtomicLRU (Just 1000)

main :: IO ()
main = do
    args <- getArgs
    cache <- newCache
    --let port = fromIntegral (read $ head args :: Int)
    sock <- listenOn $ PortNumber 4000
    print "Listening on 4000"
    sockHandler cache sock


sockHandler :: (AtomicLRU BS.ByteString HttpResponse) -> Socket -> IO ()
sockHandler cache sock = do
    (hdl, _, _) <- accept sock
    hSetBuffering hdl NoBuffering
    forkIO $ handler cache hdl
    sockHandler cache sock

handler :: (AtomicLRU BS.ByteString HttpResponse) -> Handle -> IO ()
handler cache hdl = readRequest hdl >>= 
                    \req -> processRequest cache req >>= 
                    (BS.hPut hdl . printResponse) >>= 
                    (\_ -> case continueOrClose req of 
                        Left _ -> handler cache hdl
                        Right _ -> hClose hdl)

continueOrClose :: Maybe HttpRequest -> Either String String
continueOrClose Nothing = Right "Close"
continueOrClose (Just req) 
    | proxyConnection == "keep-alive" = Left "Continue"
    | proxyConnection == "Keep-Alive" = Left "Continue"
    | otherwise = Right "Close"
    where proxyConnection = fromMaybe "close" (getArbitraryHeader (getRequestHeaders req) (pack "Proxy-Connection"))

readRequest :: Handle -> IO (Maybe HttpRequest)
readRequest hdl = parseWith (BS.hGet hdl 1) request BS.empty >>= \x -> return $ maybeResult x

badRequest :: HttpResponse
badRequest = HttpResponse (HttpResponseHead (HttpVersion ("1","1")) (HttpStatus 400 "Bad Request") [HttpHeader ("Server","HaskellProxy"),HttpHeader ("Content-Type","text/html"),HttpHeader ("Content-Length","244"),HttpHeader ("Connection","close")]) (HttpBody "<html>\r\n<head><title>400 Unparseable request</title></head>\r\n<body bgcolor=\"white\">\r\n<center><h1>400 Bad Request</h1></center>\r\n<center>HaskellProxy did not understand your request</center>\r\n<hr><center>HaskellProxy</center>\r\n</body>\r\n</html>\r\n")

badGatewayAnswer :: HttpResponse
badGatewayAnswer = HttpResponse (HttpResponseHead (HttpVersion ("1","1")) (HttpStatus 502 "Bad Gateway") [HttpHeader ("Server","HaskellProxy"),HttpHeader ("Content-Type","text/html"),HttpHeader ("Content-Length","245"),HttpHeader ("Connection","close")]) (HttpBody "<html>\r\n<head><title>502 Bad Gateway</title></head>\r\n<body bgcolor=\"white\">\r\n<center><h1>502 Bad Gateway</h1></center>\r\n<center>HaskellProxy could not receive answer to your request</center>\r\n<hr><center>HaskellProxy</center>\r\n</body>\r\n</html>\r")

isCacheable :: HttpResponse -> Bool
isCacheable res = case getCacheControl $ getResponseHeaders res of
                        NoStore         -> False
                        Unknown         -> False
                        Immutable       -> True
                        Revalidate      -> True
                        (MaxAge age)    -> age > 0
                        (SMaxAge age)   -> age > 0

processRequest :: (AtomicLRU BS.ByteString HttpResponse) -> Maybe HttpRequest -> IO HttpResponse
processRequest _ Nothing = return badRequest
processRequest cache (Just req) = do
    print "---------------request start---------------"
    print $ printRequest req
    let cacheKey = getCacheKey req
    cachedResponse <- lookup cacheKey cache

    if isNothing cachedResponse
        then do print "cache miss"
                defaultRequest cache req
        else do print "cache hit"
                let unMaybedCachedResponse = fromMaybe undefined cachedResponse -- we checked nothing before
                currentTime <- getCurrentTime
                case getCacheControl $ getResponseHeaders unMaybedCachedResponse of
                    Immutable       -> return unMaybedCachedResponse
                    Revalidate      -> validationRequest cache req unMaybedCachedResponse
                    (MaxAge age)    -> if isDateOk currentTime unMaybedCachedResponse age
                                        then do print "Timed cache OK"
                                                return unMaybedCachedResponse
                                        else do print "Timed cache old"
                                                delete cacheKey cache
                                                defaultRequest cache req
                    (SMaxAge age)   -> if isDateOk currentTime unMaybedCachedResponse age
                                        then do print "Timed cache OK"
                                                return unMaybedCachedResponse
                                        else do print "Timed cache old"
                                                delete cacheKey cache
                                                defaultRequest cache req
                                
defaultRequest :: (AtomicLRU BS.ByteString HttpResponse) -> HttpRequest -> IO HttpResponse
defaultRequest cache req = do
    let host = getHost req
    let cacheKey = getCacheKey req
    addrinfos <- getAddrInfo Nothing (Just (fst host)) (Just (snd host))
    let serveraddr = head addrinfos
    sock <- socket (addrFamily serveraddr) Stream defaultProtocol
    connect sock (addrAddress serveraddr)
    h <- socketToHandle sock ReadWriteMode
    hSetBuffering h LineBuffering
    BS.hPutNonBlocking h $ printRequest req
    msg <- parseWith ( BS.hGet h 1 ) response BS.empty
    hClose h

    let res = fromMaybe badGatewayAnswer $ maybeResult  msg
    print $ printResponseOnlyHead res

    if isSuccess res && isCacheable res
        then do insert cacheKey res cache 
                print "caching"
        else print "uncacheable"

    return res

validationRequest :: (AtomicLRU BS.ByteString HttpResponse) -> HttpRequest -> HttpResponse -> IO HttpResponse
validationRequest cache req cachedResp = do
    let host = getHost req
    let cacheKey = getCacheKey req
    let hostHeader = fromMaybe (HttpHeader ("Host", "localhost")) (getHostHeader $ getRequestHeaders req)
    let etagValue = fromMaybe "abcd" (getArbitraryHeader (getResponseHeaders cachedResp) (pack "ETag"))
    let newReq = HttpRequest (HttpRequestHead Get (HttpPath (getPath req)) (HttpVersion ("1","1")) [HttpHeader ("If-None-Match", etagValue), hostHeader]) (HttpBody "")

    addrinfos <- getAddrInfo Nothing (Just (fst host)) (Just (snd host))
    let serveraddr = head addrinfos
    sock <- socket (addrFamily serveraddr) Stream defaultProtocol
    connect sock (addrAddress serveraddr)
    h <- socketToHandle sock ReadWriteMode
    hSetBuffering h LineBuffering
    BS.hPutNonBlocking h $ printRequest newReq
    msg <- parseWith ( BS.hGet h 1 ) response BS.empty
    hClose h

    let res = fromMaybe badGatewayAnswer $ maybeResult  msg
    let status = getResponseStatus res

    if isNotModified status
        then do print "Cached version valid"
                return cachedResp
        else do print "Cached version nonvalid"
                insert cacheKey res cache
                return res

            

isDateOk :: UTCTime -> HttpResponse -> Int -> Bool
isDateOk currentTime cachedResp age = do
    let respDate = fromMaybe "Tue, 15 Nov 1994 08:12:31 GMT" (getArbitraryHeader (getResponseHeaders cachedResp) (pack "Date"))
    let parsedRespDate = fromMaybe defaultHTTPDate (parseHTTPDate respDate)
    let responseUTCTime = httpDateToUTC parsedRespDate
    let diffUTC = diffUTCTime currentTime responseUTCTime
    let diffInt = toInteger (floor diffUTC)
    let ageInteger = toInteger age

    diffInt < ageInteger

httpDateToUTC :: HTTPDate -> UTCTime
httpDateToUTC x = UTCTime (fromGregorian y m d) (secondsToDiffTime s)
    where
    y = fromIntegral $ hdYear x
    m = hdMonth x
    d = hdDay x
    s = fromIntegral $ (hdHour   x `rem` 24) * 3600
                        + (hdMinute x `rem` 60) * 60
                        + (hdSecond x `rem` 60)
    