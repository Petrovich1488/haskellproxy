{-# LANGUAGE OverloadedStrings #-}

module HttpPrinter where

import Types
import Data.Monoid
import Data.ByteString (ByteString)
import Data.ByteString.Char8 (pack)

printMethod :: HttpMethod -> ByteString
printMethod m
    | m == Get = "GET"
    | m == Post = "POST"
    | m == Head = "HEAD"

printPath :: HttpPath -> ByteString
printPath (HttpPath path) = path

printStatus :: HttpStatus -> ByteString
printStatus (HttpStatus code message) = (pack . show) code <> " " <> message

printHeaders :: HttpHeaders -> ByteString
printHeaders x = mconcat $ map printHeader x

printVersion :: HttpVersion -> ByteString
printVersion (HttpVersion (major, minor)) = "HTTP/" <> major <> "." <> minor

printHeader :: HttpHeader -> ByteString
printHeader (HttpHeader (name, value))
    | name == "Proxy-Connection" = ""
    | otherwise = name <> ": " <> value <> "\r\n"

printBody :: HttpBody -> ByteString
printBody (HttpBody b) = b

printRequestHead :: HttpRequestHead -> ByteString
printRequestHead (HttpRequestHead m p v h) =
    printMethod m <> " " <>
    printPath p <> " " <> 
    printVersion v <> "\r\n" <> 
    printHeaders h

printRequest :: HttpRequest -> ByteString
printRequest (HttpRequest h b) =
    printRequestHead h <> "\r\n" <> printBody b

printResponseHead :: HttpResponseHead -> ByteString
printResponseHead (HttpResponseHead v s h) =
    printVersion v <> " " <> 
    printStatus s <> "\r\n" <>
    printHeaders h

printResponse:: HttpResponse -> ByteString
printResponse (HttpResponse h b) =
    printResponseHead h <> "\r\n" <> printBody b

printResponseOnlyBody :: HttpResponse -> ByteString
printResponseOnlyBody (HttpResponse h b) = printBody b

printResponseOnlyHead :: HttpResponse -> ByteString
printResponseOnlyHead (HttpResponse h _) = printResponseHead h
