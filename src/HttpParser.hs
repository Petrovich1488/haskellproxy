{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

-- https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html

module HttpParser where 

import Prelude hiding (takeWhile, take, concat)
import Types
import Control.Applicative
import Control.Monad
import Data.Functor
import Data.List.Split hiding (chunk, sepBy)
import Data.List (intercalate)
import Data.Maybe
import Data.Monoid
import Numeric (readHex)
import Data.ByteString (ByteString)
import Data.ByteString.Char8 (readInt, putStr, pack, unpack, findSubstring, concat)
import Data.Attoparsec.ByteString (Parser, parse, parseOnly, takeWhile, take,
                                   notInClass, takeByteString, sepBy, satisfy,
                                   Result, parseWith, inClass)
import Data.Attoparsec.ByteString.Char8 (space, isDigit_w8, stringCI, skipSpace, sepBy1', char, decimal, parseOnly)

eol = stringCI "\r\n"
noEol = takeWhile (\w -> not (w == 13 || w == 10))
digit = takeWhile isDigit_w8
hexDigit = takeWhile (inClass "0123456789abcdefABCDEF")
isHexDigit_w8 w = w - 48 <= 9 || w - 65 <= 5

chunk :: Parser ByteString
chunk = do
     len <- hexDigit
     (eol *> take (hexToInt len)) >>= 
        (\content -> return (len <> (pack "\r\n") <> content <> (pack "\r\n")))

chunks :: Parser [ByteString]
chunks = chunk `sepBy` eol

hexToInt :: ByteString -> Int
hexToInt str = fst $ head $ readHex $ unpack str

method :: Parser HttpMethod
method = get <|> post <|> head
    where get = stringCI "GET" $> Get
          post = stringCI "POST" $> Post
          head = stringCI "HEAD" $> Head

path :: Parser HttpPath
path = HttpPath <$> takeWhile (notInClass " \n\r")          

status :: Parser HttpStatus
status = constructStatus . toInt <$> digit
    where toInt y = fst $ fromMaybe (0, "") (readInt y)

version :: Parser HttpVersion
version = HttpVersion <$> liftA2 (,) (stringCI "HTTP/" *> digit) (stringCI "." *> digit)

header :: Parser HttpHeader
header = HttpHeader <$> liftA2 (,) takeLabel (stringCI ": " *> noEol)
    where takeLabel = takeWhile (notInClass ": \n\r")

headers :: Parser HttpHeaders
headers = header `sepBy` eol    

data BodyLength = Length Int | Chunked

body :: BodyLength -> Parser HttpBody
body (Length x) = HttpBody <$> take x
body Chunked = HttpBody <$> fmap mconcat chunks <* eol

request :: Parser HttpRequest
request = do
    reqHead <- requestHead 
    eol
    let bodyLength = getReqBodyLength reqHead
    reqBody <- body bodyLength
    return (HttpRequest reqHead reqBody)

requestHead :: Parser HttpRequestHead
requestHead = HttpRequestHead
    <$> method
    <*> (space *> path)
    <*> (space *> version)
    <*> (eol *> headers <* eol)

responseHead :: Parser HttpResponseHead
responseHead = HttpResponseHead
    <$> version
    <*> (space *> status)
    <* (space *> noEol)
    <*> (eol *> headers <* eol)    

response :: Parser HttpResponse
response = do
    resHead <- responseHead
    eol
    let bodyLength = getResBodyLength resHead
    resBody <- body bodyLength
    return (HttpResponse resHead resBody)

getReqBodyLength :: HttpRequestHead -> BodyLength
getReqBodyLength (HttpRequestHead method path version headers)
    | method == Get = Length 0
    | hasContentLength headers = Length (getContentLength headers)
    | isChunkedEncoding headers = Chunked
    | otherwise = Length 0

getResBodyLength :: HttpResponseHead -> BodyLength
getResBodyLength (HttpResponseHead version status headers)
    | hasContentLength headers = Length (getContentLength headers)
    | isRedirectStatus status = Length 0
    | isChunkedEncoding headers = Chunked
    | otherwise = Length 0    

hasContentLength :: HttpHeaders -> Bool
hasContentLength [] = False
hasContentLength (HttpHeader (header,value):xs)
    | header == "Content-Length" = True
    | otherwise = hasContentLength xs

getContentLength :: HttpHeaders -> Int
getContentLength [] = 0
getContentLength (HttpHeader (header,value):xs)
    | header == "Content-Length" = fst (fromMaybe (0, "") (readInt value))
    | otherwise = getContentLength xs

getArbitraryHeader :: HttpHeaders -> ByteString -> Maybe ByteString
getArbitraryHeader [] _ = Nothing
getArbitraryHeader (HttpHeader (header,value):xs) name
    | header == name = Just value
    | otherwise = getArbitraryHeader xs name

getHostHeader :: HttpHeaders -> Maybe HttpHeader
getHostHeader [] = Nothing
getHostHeader (HttpHeader (header,value):xs)
    | header == "Host" = Just (HttpHeader (header, value))
    | header == "host" = Just (HttpHeader (header, value))
    | otherwise = getHostHeader xs

getRequestHeaders :: HttpRequest -> HttpHeaders
getRequestHeaders (HttpRequest (HttpRequestHead _ _ _ headers) _) = headers

getResponseHeaders :: HttpResponse -> HttpHeaders
getResponseHeaders (HttpResponse (HttpResponseHead _ _ headers) _) = headers

isChunkedEncoding :: HttpHeaders -> Bool
isChunkedEncoding [] = False
isChunkedEncoding (HttpHeader (x,y):xs)
    | x == "Transfer-Encoding" && y == "chunked" = True
    | otherwise = isChunkedEncoding xs

isSuccess :: HttpResponse -> Bool
isSuccess (HttpResponse (HttpResponseHead _ (HttpStatus code _) _) _) 
    = code == 200

getPath :: HttpRequest -> ByteString
getPath (HttpRequest (HttpRequestHead _ (HttpPath path) _ _) _) = path

getHost :: HttpRequest -> (String, String)
getHost req = case getArbitraryHeader (getRequestHeaders req) (pack "Host") of
                Just a -> (head $ splitOn ":" $ unpack a, getPort $ unpack a)
                _ -> ("localhost", "80")

getCacheKey :: HttpRequest -> ByteString
getCacheKey req = concat [pack (fst host), pack ":", pack (snd host),pack "/", path]
                    where path = getPath req
                          host = getHost req

getResponseStatus :: HttpResponse -> HttpStatus
getResponseStatus (HttpResponse (HttpResponseHead _ httpStatus _) _) = httpStatus

getPort :: String -> String
getPort hostString
    | null hostSplitted = "80"
    | otherwise = head hostSplitted
    where hostSplitted = tail $ splitOn ":" hostString
                                                                                    
getCacheControl :: HttpHeaders -> CacheControl
getCacheControl [] = Unknown
getCacheControl (HttpHeader (x,y):xs)
    | x == "Cache-Control" = either (const NoStore) id $ parseOnly cacheControlHeaderParser y
    | otherwise = getCacheControl xs    

cacheControlHeaderParser :: Parser CacheControl
cacheControlHeaderParser = liftM mconcat $ directive `sepBy1'` (char ',')
    where   directive = skipSpace >> (noStore <|> immutable <|> maxAge <|> sMaxAge <|> other) <* skipSpace
            noStore = (stringCI "no-store" <|> stringCI "private") >> return NoStore
            immutable = stringCI "immutable" >> return Immutable
            revalidate = (stringCI "must-revalidate" <|> stringCI "proxy-revalidate") >> return Revalidate
            maxAge = stringCI "max-age" >> num >>= return . MaxAge
            sMaxAge = stringCI "s-maxage" >> num >>= return . SMaxAge
            other = takeWhile (notInClass ",") >> return Unknown
            num = skipSpace >> char '=' >> (decimal <|> (char '"' >> decimal <* char '"')) <* skipSpace    
